# Pan and Zoom

Testing pan and zoom, learning how Screen To World Space and World To Screen Space works.

## How to run

First install the required packages with

```shell
yarn
```

For `npm` use

```shell
npm i
```

### Development

The command `yarn` can be subtitute with `npm`.

This runs with server on.

```shell
yarn dev
```

### Build for production

Before building it, do `prebuild`, this will clean up the build folder.

```shell
yarn prebuild
```

Build the project with

```shell
yarn build
```
