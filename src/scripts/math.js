function lerp(a, b, n) {
  return (1 - n) * a + n * b
}

function map(v, a, b, c, d) {
  return ((v - a) * (d - c)) / (b - a) + c
}

function constrain(value, min, max) {
  if (value < min) return min
  else if (value > max) return max
  else return value
}

function length2Vectors(vector0, vector1) {
  return Math.sqrt(
    Math.pow(vector1[0] - vector0[0], 2) + Math.pow(vector1[1] - vector0[1], 2)
  )
}

export { lerp, map, constrain, length2Vectors }
