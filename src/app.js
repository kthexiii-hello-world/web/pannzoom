import DebugLogger from './scripts/DebugLogger'
import { map, constrain, length2Vectors } from './scripts/math'

const canvas = document.createElement('canvas')
document.getElementById('renderer-container').appendChild(canvas)
canvas.style.height = '100%'
canvas.style.width = '100%'
canvas.id = 'renderer'

// Add events
window.addEventListener('resize', handleWindowResize)
window.addEventListener('keydown', handleKeyDown)
canvas.addEventListener('mousedown', handleMDown)
canvas.addEventListener('mousemove', handleMove)
canvas.addEventListener('mouseup', handleMUp)
canvas.addEventListener('mouseenter', handleMEnter)
canvas.addEventListener('mouseleave', handleMLeave)
canvas.addEventListener('wheel', handleMWheel)

canvas.addEventListener('touchstart', handleTStart)
canvas.addEventListener('touchmove', handleTMove)
canvas.addEventListener('touchcancel', handleTCancel)
canvas.addEventListener('touchend', handleTEnd)

const DEBUG = new DebugLogger(
  {
    loopStatusLog: true,
    overlay: true,
    graph: true,
  },
  stopStart
)

const cx = canvas.getContext('2d')
resize(cx) // Resize the canvas to fit the current frame

let autoStart = true
let animationID
let isMouseDown
let mousePosition = [0, 0]
let mouseStartPan = [0, 0]
let mouseWorldPos = [0, 0]
let mouseWorldPosZoom = [0, 0]
let touchLists = []

let initialLength = 0
let deltaLength = 0
let currentLength = 0

let offset = [-cx.canvas.width / 2, -cx.canvas.height / 2]
let scale = [1, 1]

let currentTime = 0
let oldTime = 0
let deltaTime = 0

main()

function main() {
  render()

  DEBUG.loopStatus(autoStart)
  if (autoStart) animate()
}

function animate() {
  update()
  render()

  DEBUG.update()

  animationID = requestAnimationFrame(animate)
}

function update() {
  if (isMouseDown) {
    offset[0] -= (mousePosition[0] - mouseStartPan[0]) / scale[0]
    offset[1] -= (mousePosition[1] - mouseStartPan[1]) / scale[1]

    mouseStartPan[0] = mousePosition[0]
    mouseStartPan[1] = mousePosition[1]
  }

  currentTime = performance.now()
  deltaTime = performance.now() - oldTime
  oldTime = currentTime
}

function render() {
  background(cx, '#161616')
  cx.strokeStyle = '#ffffff'

  let worldPos = [0, 0]
  let screenPos = [0, 0]

  worldToScreenSpace(worldPos, screenPos)
  cx.lineWidth = 1 * scale[0]
  const gridSize = 10
  const cellSize = 25
  drawGrid(
    cx,
    screenPos[0] - (gridSize * cellSize * scale[0]) / 2,
    screenPos[1] - (gridSize * cellSize * scale[1]) / 2,
    gridSize,
    cellSize * scale[0]
  )

  if (touchLists.length > 1) {
    cx.lineWidth = 2 * devicePixelRatio
    const point0 = [touchLists[0][0], touchLists[0][1]]
    const point1 = [touchLists[1][0], touchLists[1][1]]

    drawLine(cx, point0, point1)
    cx.stroke()

    cx.fillStyle = '#f54269'
    drawCircle(
      cx,
      point1[0] + (point0[0] - point1[0]) / 2,
      point1[1] + (point0[1] - point1[1]) / 2,
      20
    )
    cx.fill()

    cx.fillStyle = '#42aaf5'
    drawCircle(cx, point0[0], point0[1], 50)
    cx.fill()
    drawCircle(cx, point1[0], point1[1], 50)
    cx.fill()
  }
}

/**
 * Takes in World space and convert it to Screen space
 * @param {Array} worldSpace Input World Space. Array of 2 numbers, the elements are x, y
 * @param {Array} screenSpace Output Screen SpaceArray. Array of 2 numbers, the elements are x, y
 */
function worldToScreenSpace(worldSpace, screenSpace) {
  screenSpace[0] = (worldSpace[0] - offset[0]) * scale[0]
  screenSpace[1] = (worldSpace[1] - offset[1]) * scale[1]
}

/**
 * Takes in Screen space and convert it to World space
 * @param {Array} screenSpace Output Screen SpaceArray. Array of 2 numbers, the elements are x, y
 * @param {Array} worldSpace Input World Space. Array of 2 numbers, the elements are x, y
 */
function screenToWorldSpace(screenSpace, worldSpace) {
  worldSpace[0] = screenSpace[0] / scale[0] + offset[0]
  worldSpace[1] = screenSpace[1] / scale[1] + offset[1]
}

function stopStart() {
  if (animationID) {
    cancelAnimationFrame(animationID)
    animationID = null
  } else {
    animate()
  }

  DEBUG.loopStatus(animationID !== null)
}

function drawLine(context, origin, vector) {
  context.beginPath()
  context.moveTo(origin[0], origin[1])
  context.lineTo(vector[0], vector[1])
  context.closePath()
}

function drawCircle(context, x, y, radius) {
  context.beginPath()
  context.arc(x, y, radius, 0, Math.PI * 2)
  context.closePath()
}

function background(context, color) {
  context.fillStyle = color
  context.fillRect(0, 0, context.canvas.width, context.canvas.height)
  context.fill()
}

function drawGrid(context, x, y, length, size) {
  for (let i = 0; i <= length; i++) {
    context.beginPath()
    context.moveTo(i * size + x, 0 + y)
    context.lineTo(i * size + x, length * size + y)
    context.closePath()
    context.stroke()

    context.beginPath()
    context.moveTo(0 + x, i * size + y)
    context.lineTo(length * size + x, i * size + y)
    context.closePath()
    context.stroke()
  }
}

function resize(context) {
  const displayWidth = Math.floor(context.canvas.clientWidth * devicePixelRatio)
  const displayHeight = Math.floor(
    context.canvas.clientHeight * devicePixelRatio
  )

  // Check if the canvas is not the same size.
  if (
    context.canvas.width != displayWidth ||
    context.canvas.height != displayHeight
  ) {
    // Make the canvas the same size
    context.canvas.width = displayWidth
    context.canvas.height = displayHeight
  }
}

function handleWindowResize(event) {
  resize(cx)
}
function handleKeyDown(event) {
  if (event.code === 'Space') stopStart()
  if (event.code === 'KeyC') console.clear()
}

function handleMDown(event) {
  if (event.button == 0) {
    mouseStartPan = [...mousePosition]
    isMouseDown = true
  }
}

function handleMove(event) {
  mousePosition = [
    event.clientX * devicePixelRatio,
    event.clientY * devicePixelRatio,
  ]
}

function handleMUp(event) {
  if (event.button == 0) isMouseDown = false
}

function handleMEnter(event) {}

function handleMLeave(event) {
  isMouseDown = false
}

function handleMWheel(event) {
  screenToWorldSpace(mousePosition, mouseWorldPos)

  scale[0] *= constrain(map(event.deltaY, -15, 15, 0.5, 1.5), 0.9, 1.1)
  scale[1] *= constrain(map(event.deltaY, -15, 15, 0.5, 1.5), 0.9, 1.1)

  screenToWorldSpace(mousePosition, mouseWorldPosZoom)

  offset[0] += mouseWorldPos[0] - mouseWorldPosZoom[0]
  offset[1] += mouseWorldPos[1] - mouseWorldPosZoom[1]
}

const zoomRange = 150
function zoom(delta) {
  screenToWorldSpace(mousePosition, mouseWorldPos)

  scale[0] *= constrain(map(delta, -zoomRange, zoomRange, 0.5, 1.5), 0.9, 1.1)
  scale[1] *= constrain(map(delta, -zoomRange, zoomRange, 0.5, 1.5), 0.9, 1.1)

  screenToWorldSpace(mousePosition, mouseWorldPosZoom)

  offset[0] += mouseWorldPos[0] - mouseWorldPosZoom[0]
  offset[1] += mouseWorldPos[1] - mouseWorldPosZoom[1]
}

function handleTStart(event) {
  event.preventDefault()
  switch (event.touches.length) {
    case 1:
      mouseStartPan = [
        event.touches[0].clientX * devicePixelRatio,
        event.touches[0].clientY * devicePixelRatio,
      ]
      touchLists = []
      break
    case 2:
      touchLists = [
        [
          event.touches[0].clientX * devicePixelRatio,
          event.touches[0].clientY * devicePixelRatio,
        ],
        [
          event.touches[1].clientX * devicePixelRatio,
          event.touches[1].clientY * devicePixelRatio,
        ],
      ]
      mouseStartPan = [
        touchLists[0][0] + (touchLists[1][0] - touchLists[0][0]) / 2,
        touchLists[0][1] + (touchLists[1][1] - touchLists[0][1]) / 2,
      ]
      initialLength = length2Vectors(touchLists[0], touchLists[1])

      break
    default:
      isMouseDown = false
      break
  }
}

function handleTMove(event) {
  event.preventDefault()
  isMouseDown = true

  switch (event.touches.length) {
    case 1:
      mousePosition[0] = event.touches[0].clientX * devicePixelRatio
      mousePosition[1] = event.touches[0].clientY * devicePixelRatio
      break

    case 2:
      touchLists = [
        [
          event.touches[0].clientX * devicePixelRatio,
          event.touches[0].clientY * devicePixelRatio,
        ],
        [
          event.touches[1].clientX * devicePixelRatio,
          event.touches[1].clientY * devicePixelRatio,
        ],
      ]
      mousePosition[0] =
        touchLists[0][0] + (touchLists[1][0] - touchLists[0][0]) / 2
      mousePosition[1] =
        touchLists[0][1] + (touchLists[1][1] - touchLists[0][1]) / 2

      currentLength = length2Vectors(touchLists[0], touchLists[1])
      deltaLength = currentLength - initialLength
      initialLength = currentLength
      zoom(deltaLength)
      break

    default:
      isMouseDown = false
      break
  }

  console.log(event.touches.length, touchLists.length)
}
function handleTCancel(event) {
  isMouseDown = false
  touchLists = []
}
function handleTEnd(event) {
  isMouseDown = false
  touchLists.pop()

  if (touchLists.length === 1) {
    mouseStartPan = [
      event.touches[0].clientX * devicePixelRatio,
      event.touches[0].clientY * devicePixelRatio,
    ]
  }
}
